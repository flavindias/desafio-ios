//
//  Onwer.swift
//  GitHub Request
//
//  Created by Flaviano Dias Fontes on 11/10/17.
//  Copyright © 2017 Flaviano Dias Fontes. All rights reserved.
//

import Foundation
import ObjectMapper

class Owner: Mappable {
    var id: Int?
    var username: String?
    var photo: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        username <- map["login"]
        photo <- map["avatar_url"]
    }

}
