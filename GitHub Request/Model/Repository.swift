//
//  Project.swift
//  GitHub Request
//
//  Created by Flaviano Dias Fontes on 11/10/17.
//  Copyright © 2017 Flaviano Dias Fontes. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class Repository: Mappable {
    var id: Int!
    var name: String?
    var description: String?
    var stargazers: Int?
    var watchers: Int?
    var language: String?
    var forks: Int?
    var owner: Owner?
    var pullRequests: [PullRequest]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        stargazers <- map["stargazers_count"]
        watchers <- map["watchers_count"]
        language <- map["language"]
        forks <- map["forks"]
        owner <- map["owner"]
    }
    
    
    /*
     *   Método para conversão de datas advindas do servidor para DateComponents
     *   Entrada: String formatada no JSON
     *   Saída: Objeto do tipo DateComponetns com ano, mes dia hora e minuto
     */
    func stringToDateComponents(_ dateString: String) -> DateComponents?{
        
        let formatter1 = DateFormatter()
        
        formatter1.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZ"
        //        formatter1.locale = NSLocale.init(localeIdentifier: "enUSPOSIXLocale") as Locale!
        //        formatter1.timeZone = TimeZone(abbreviation: "UTC")
        let value = formatter1.date(from: dateString)
        if value != nil{
            let date = value! as Date
            let comp = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: date)
            return comp
        }
        else{
            return nil
        }
    }
}
