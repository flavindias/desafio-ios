//
//  PullRequest.swift
//  GitHub Request
//
//  Created by Flaviano Dias Fontes on 12/10/17.
//  Copyright © 2017 Flaviano Dias Fontes. All rights reserved.
//

import Foundation
import ObjectMapper

class PullRequest: Mappable{
    
    
    var id: Int!
    var url: String?
    var owner: Owner?
    var description: String?
    var title: String?
    var open: String?
    var created: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        url <- map["html_url"]
        owner <- map["user"]
        description <- map["body"]
        title <- map["title"]
        open <- map["state"]
        created <- map["created_at"]
    }
    
}
