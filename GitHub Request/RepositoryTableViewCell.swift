//
//  RepositoryTableViewCell.swift
//  GitHub Request
//
//  Created by Flaviano Dias Fontes on 11/10/17.
//  Copyright © 2017 Flaviano Dias Fontes. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var nameTitle: UILabel!
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var starImage: UIImageView!
    @IBOutlet weak var starCount: UILabel!
    @IBOutlet weak var forksImage: UIImageView!
    @IBOutlet weak var forksCount: UILabel!
    @IBOutlet weak var watchImage: UIImageView!
    @IBOutlet weak var watchCount: UILabel!
    @IBOutlet weak var ownerUsername: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.photoImage.backgroundColor = UIColor.white
        self.photoImage.layer.borderColor = UIColor(red:0.14, green:0.16, blue:0.18, alpha:1.0).cgColor
        self.photoImage.layer.borderWidth = 1.5
        self.photoImage.layer.cornerRadius = self.photoImage.frame.size.width / 2
        self.photoImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.photoImage.backgroundColor = UIColor.white
        self.photoImage.layer.borderColor = UIColor(red:0.14, green:0.16, blue:0.18, alpha:1.0).cgColor
        self.photoImage.layer.borderWidth = 1.5
        self.photoImage.layer.cornerRadius = self.photoImage.frame.size.width / 2
        self.photoImage.clipsToBounds = true
    }

}
