//
//  Reachability.swift
//  GitHub Request
//
//  Created by Flaviano Dias Fontes on 20/10/17.
//  Copyright © 2017 Flaviano Dias Fontes. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class Reachability {
    
    func getPullRequests(_ repository: Repository, completion: @escaping (_ success: Bool, _ pullRequests: [PullRequest], _ message: String) -> ()){
        var user = ""
        var repo = ""
        if let owner = repository.owner{
            if let username = owner.username{
                user = username
            }
        }
        if let name = repository.name{
            repo = name
        }
        
        Alamofire.request("https://api.github.com/repos/\(user)/\(repo)/pulls")
            .validate()
            .responseArray(completionHandler: { (response: DataResponse<[PullRequest]>) in
                if let pullRequestArray = response.result.value{
                    completion(true, pullRequestArray, "")
                }
                
            })
    }
    
    /*
     *  Função que captura os repositórios do git
     *  Parametros de entrada: página atual ex: 1, linguagem ex: Java, ordenação ex: stars
     *  Retorno assíncrono: sucesso booleano, listagem de repositórios, mensagem ex: erro
     */
    func getProjects(_ page: Int, _ language: String, _ sort: String, completion: @escaping (_ success: Bool, _ repositories: [Repository], _ message: String) -> ()) {
        Alamofire.request("https://api.github.com/search/repositories?q=language:\(language)&sort=stars&page=\(page)")
            .validate()
            .responseArray(queue: nil, keyPath: "items", context: nil, completionHandler: { (response: DataResponse<[Repository]>) in
                
                print(response)
                switch response.result{
                case .success:
                    if let repositoryArray = response.result.value{
                        for rep in repositoryArray{
                            print(rep.id)
                        }
                        completion(true, repositoryArray, "")
                    }
                case .failure:
                    completion(false, [Repository](), "")
                }
                
                
                
            })
        
    }
}
