//
//  PullRequestViewController.swift
//  GitHub Request
//
//  Created by Flaviano Dias Fontes on 15/10/17.
//  Copyright © 2017 Flaviano Dias Fontes. All rights reserved.
//

import UIKit
import EZLoadingActivity

class PullRequestViewController: UIViewController {
    var reachability = Reachability()
    var repository: Repository!
    var pullRequests = [PullRequest]()
    var selectedPullRequest: PullRequest!
    var currentPage: Int = 1
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var ownerUsername: UILabel!
    @IBOutlet weak var starImage: UIImageView!
    @IBOutlet weak var starCount: UILabel!
    @IBOutlet weak var forksImage: UIImageView!
    @IBOutlet weak var forksCount: UILabel!
    @IBOutlet weak var watchImage: UIImageView!
    @IBOutlet weak var watchCount: UILabel!
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.currentPage = 1
        self.pullRequests = [PullRequest]()
        self.getData(self.currentPage)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController!.navigationBar.barStyle = .black
        self.navigationController!.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.backgroundView.backgroundColor = UIColor(red:0.20, green:0.22, blue:0.24, alpha:1.0)
        self.view.backgroundColor = UIColor(red:0.20, green:0.22, blue:0.24, alpha:1.0)
        
        //    Usar o código abaixo para substituir o botão de voltar por uma imagem e executar esse código
        self.navigationItem.setHidesBackButton(true, animated: true)
        let backButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "backIcon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.goBack))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        if let name = repository.name{
            self.title = name
        }
        else{
            self.title = NSLocalizedString("PullRequestViewController.title", comment: "App Title")
        }
        self.tableView.delegate = self
        self.tableView.dataSource = self
        if let stars = repository.stargazers{
            self.starCount.text = "\(stars)"
        }
        if let fork = repository.forks{
            self.forksCount.text = "\(fork)"
        }
        if let watch = repository.watchers{
            self.watchCount.text = "\(watch)"
        }
        if let owner = repository.owner{
            if let photo = owner.photo{
                if let url = Foundation.URL(string: photo){
                    self.photoImage.af_setImage(withURL: url)
                }
            }
            if let username = owner.username{
                self.ownerUsername.text = username
            }
        }

    }
    override func viewDidLayoutSubviews() {
        self.photoImage.backgroundColor = UIColor.white
        self.photoImage.layer.borderColor = UIColor.white.cgColor
        self.photoImage.layer.borderWidth = 3.0
        self.photoImage.layer.cornerRadius = self.photoImage.frame.size.width / 2
        self.photoImage.clipsToBounds = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func getData(_ page: Int){
        if self.currentPage == 1{
            EZLoadingActivity.show(NSLocalizedString("PullRequestViewController.alert.message", comment: ""), disableUI: true)
        }
        
        self.reachability.getPullRequests(self.repository) { (success, pullRequests, message) in
            if success{
                
                if self.currentPage != 1{
                    self.pullRequests.append(contentsOf: pullRequests)
                }
                else{
                    EZLoadingActivity.hide(true, animated: true)
                    self.pullRequests = pullRequests
                }
                self.tableView.reloadData()
            }
            else{
                print(message)
            }
        }
    }
    @objc func goBack(){
        self.navigationController!.popViewController(animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToWebView"{
            let view = segue.destination as! PullRequestWebViewController
            view.repository = self.repository
            view.pullRequest = self.selectedPullRequest
        }
    }
}
extension PullRequestViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.currentPage = self.currentPage + 1
        if (indexPath.row == self.pullRequests.count - 1) {
            self.getData(self.currentPage)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedPullRequest = self.pullRequests[indexPath.row]
        self.performSegue(withIdentifier: "segueToWebView", sender: nil)
//        if let urlString = self.pullRequests[indexPath.row].url{
//            guard let url = URL(string: urlString) else {
//                return //garantindo pra não dar crash
//            }
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            } else {
//                UIApplication.segueToWebViewshared.openURL(url)
//            }
//        }
    }
}
extension PullRequestViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let pullRequest = self.pullRequests[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: "pullRequestCell", for: indexPath) as? PullRequestTableViewCell{
            if let title = pullRequest.title{
                cell.titleLabel.text = title
            }
            if let desc = pullRequest.description{
                cell.descriptionTitle.text = desc
            }
            if let dateComp = repository.stringToDateComponents(pullRequest.created!){
                let date = Calendar.current.date(from: dateComp)
                let formatter = DateFormatter()
                formatter.dateStyle = DateFormatter.Style.short
                formatter.timeStyle = .short
                cell.dateTitle.text = formatter.string(from: date!)
            }
            if let owner = pullRequest.owner{
                if let photo = owner.photo{
                    if let url = Foundation.URL(string: photo){
                        cell.photoImage.af_setImage(withURL: url)
                    }
                }
                if let username = owner.username{
                    cell.ownerUsername.text = username
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.pullRequests.count
    }
    
}
