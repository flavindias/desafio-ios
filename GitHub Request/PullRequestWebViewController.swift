//
//  PullRequestWebViewController.swift
//  GitHub Request
//
//  Created by Flaviano Dias Fontes on 15/10/17.
//  Copyright © 2017 Flaviano Dias Fontes. All rights reserved.
//

import UIKit
import EZLoadingActivity

class PullRequestWebViewController: UIViewController {
    var pullRequest: PullRequest!
    var repository: Repository!
    @IBOutlet weak var webView: UIWebView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red:0.20, green:0.22, blue:0.24, alpha:1.0)
        if let name = repository.name{
            self.title = name
        }
        else{
            self.title = NSLocalizedString("GitHub Request", comment: "App Title")
        }
        self.navigationItem.setHidesBackButton(true, animated: true)
        
        let backButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "backIcon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.goBack))
        let browserButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "browserIcon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.goToSafari))
        self.navigationItem.rightBarButtonItem = browserButton
        self.navigationItem.leftBarButtonItem = backButton
        self.webView.delegate = self
        
        // Do any additional setup after loading the view.
        if let urlString = pullRequest.url{
            if let url = URL(string: urlString){
                let requestObj = NSMutableURLRequest(url: url)
                self.webView.loadRequest(requestObj as URLRequest)
            }
        }
    }
    @objc func goToSafari(){
        let uiAlertGoSafari = UIAlertController(title: NSLocalizedString("PullRequestWebViewController.uiAlertGoSafari.title", comment: ""), message: NSLocalizedString("PullRequestWebViewController.uiAlertGoSafari.message", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        let uiActionGoSafari = UIAlertAction(title: NSLocalizedString("PullRequestWebViewController.uiActionGoSafari.title", comment: ""), style: UIAlertActionStyle.default) { (action) in
            if let url = self.pullRequest.url{
                guard let url = URL(string: url) else {
                    return //garantindo pra não dar crash
                }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        let uiActionCancelGoSafari = UIAlertAction(title: NSLocalizedString("PullRequestWebViewController.uiActionCancelGoSafari.title", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        uiAlertGoSafari.addAction(uiActionGoSafari)
        uiAlertGoSafari.addAction(uiActionCancelGoSafari)
        self.present(uiAlertGoSafari, animated: true, completion: nil)
        
    }
    @objc func goBack(){
        self.navigationController!.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PullRequestWebViewController: UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
        EZLoadingActivity.show(NSLocalizedString("PullRequestWebViewController.alert.message", comment: ""), disableUI: true)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        EZLoadingActivity.hide(true, animated: true)
    }
}
