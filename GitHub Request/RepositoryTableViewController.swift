//
//  RepositoryTableViewController.swift
//  GitHub Request
//
//  Created by Flaviano Dias Fontes on 11/10/17.
//  Copyright © 2017 Flaviano Dias Fontes. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import EZLoadingActivity
import Alamofire
import AlamofireImage

class RepositoryTableViewController: UITableViewController {
    var reachability = Reachability()
    var repositories = [Repository]()
    var currentPage: Int = 1
    var selectedRepository: Repository?
    let cellBuffer: CGFloat = 2
    var selectedLanguage: String = "Java"
    var sort: String = "stars"
    
    override func viewWillAppear(_ animated: Bool) {
        self.currentPage = 1
        self.repositories = [Repository]()
        self.getData(self.currentPage, self.selectedLanguage, sortedBy: self.sort)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        self.navigationController!.navigationBar.barStyle = .black
                self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
                self.navigationController?.navigationBar.shadowImage = UIImage()
                self.navigationController?.navigationBar.isTranslucent = true
        self.tableView.backgroundColor = UIColor(red:0.20, green:0.22, blue:0.24, alpha:1.0)
        self.title = NSLocalizedString("GitHub Request", comment: "App Title")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let menuButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.filter))
        self.navigationItem.leftBarButtonItem = menuButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    @objc func filter(){
        let doneBlock: ActionStringDoneBlock = {
            picker, value, index in
            
            if let lang = index{
                self.selectedLanguage = String(describing: lang)
                self.currentPage = 1
                self.getData(self.currentPage, self.selectedLanguage, sortedBy: self.sort)
            }
            return
        }
        let cancelBlock: ActionStringCancelBlock = {
            _ in return
        }
        let languagesArray = ["C", "PHP", "Java", "Swift"]
        
        ActionSheetStringPicker.show(withTitle: NSLocalizedString("RepositoryTableViewController.ActionSheetStringPicker.title", comment: ""), rows: languagesArray, initialSelection: languagesArray.index(of: self.selectedLanguage)!, doneBlock: doneBlock, cancel: cancelBlock, origin: self.navigationItem.leftBarButtonItem)
    }
    func getData(_ page: Int, _ selectedLanguage: String, sortedBy: String) {
        EZLoadingActivity.show(NSLocalizedString("RepositoryTableViewController.alert.message", comment: ""), disableUI: true)
        self.reachability.getProjects(page, selectedLanguage, sortedBy) { (success, repositories, message) in
            if success{
                EZLoadingActivity.hide(true, animated: true)
                if self.currentPage != 1{
                    self.repositories.append(contentsOf: repositories)
                }
                else{
                    
                    self.repositories = repositories
                }
                
                self.tableView.reloadData()
            }
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.repositories.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let repository = self.repositories[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: "repositoryCell", for: indexPath) as? RepositoryTableViewCell{
            if let name = repository.name{
                cell.nameTitle.text = name
            }
            if let desc = repository.description{
                cell.descriptionTitle.text = desc
            }
            if let stars = repository.stargazers{
                cell.starCount.text = "\(stars)"
            }
            if let fork = repository.forks{
                cell.forksCount.text = "\(fork)"
            }
            if let watch = repository.watchers{
                cell.watchCount.text = "\(watch)"
            }
            if let owner = repository.owner{
                if let photo = owner.photo{
                    if let url = Foundation.URL(string: photo){
                        cell.photoImage.af_setImage(withURL: url)
                    }
                        
            
                    
                }
                if let username = owner.username{
                    cell.ownerUsername.text = username
                }
            }
            return cell
            
        }
        return UITableViewCell()
    }
//    Função responsável pelo scroll infinito
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let bottom = scrollView.contentSize.height - scrollView.frame.size.height
        let scrollPosition = scrollView.contentOffset.y
        let buffer: CGFloat = self.cellBuffer * 120
//        checa se é o fim do scroll
        if scrollPosition > bottom - buffer{
            self.currentPage = self.currentPage + 1
            self.getData(self.currentPage, self.selectedLanguage, sortedBy: self.sort)
            
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedRepository = self.repositories[indexPath.row]
        self.performSegue(withIdentifier: "segueToPullRequest", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToPullRequest"{
            let view = segue.destination as! PullRequestViewController
            view.repository = self.selectedRepository
        }
    }
    
}
